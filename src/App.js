import './App.css';

import {useState, useEffect} from 'react';

import AppNavBar from './components/AppNavBar.js';
import Register from './pages/Register.js';
import Home from './pages/Home.js';
import Logout from './pages/Logout.js';
import Login from './pages/Login.js';
import AddRooms from './pages/AddRooms.js';
import PageNotFound from './pages/PageNotFound.js';
import Rooms from './pages/Rooms.js'
import RoomSearch from './pages/RoomSearch.js'


import UpdateRooms from './components/UpdateRooms.js'
import Archive from './components/Archive.js'
import AddToCart from './components/AddToCart.js'






import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {UserProvider} from './UserContext.js';

function App() {

  const [user,setUser]= useState(null)

  // useEffect(()=>{
  //   console.log(user);
  // },[user])

  const unSetUser = () =>{
    localStorage.clear();
    
  }

  useEffect(()=>{


   
    fetch(`http://localhost:4001/user/myProfile`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      if(localStorage.getItem('token') !== null){
        setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })

      }else{
        setUser(null);
      }
    })
  },[])


  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
          <Route path="/" element = {<Home/>}/>
          {/*<Route path="/courses" element = {<Courses/>}/>*/}
          <Route path="/login" element = {<Login/>}/>
          <Route path="/logout" element = {<Logout/>}/>
          <Route path="/register" element = {<Register/>}/>
          <Route path="*" element = {<PageNotFound/>}/>
          <Route path="/roomsView" element = {<Rooms/>}/>
          <Route path="/addRooms" element = {<AddRooms/>}/>
          <Route path="/UpdateRooms/:roomId" element = {<UpdateRooms/>}/>
          <Route path="/archive/:roomId/:status" element = {<Archive/>}/>
          <Route path="/roomsearch" element = {<RoomSearch/>}/>
          <Route path="/addtocart" element = {<AddToCart/>}/>
          {/*<Route path="/test" element = {<Test/>}/>*/}

          {/*<Route path="/course/:courseId" element = {<CourseView/>}/>*/}
        </Routes>
      </Router>
    </UserProvider>
    
  );
}

export default App;
