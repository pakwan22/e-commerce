// import {Card, Button} from 'react-bootstrap';
import {Card, Row, Col} from 'react-bootstrap';

export default function Highlights () {
	return(
		<Row className = "mt-5">
		{/*First Card*/}
			<Col className = "col-md-4 col-10 mx-auto m-md-0 m-1">
				<Card className = "cardHighlight">
			      <Card.Body>
			       <img
         			 src="https://i.ibb.co/MsHV7VM/ovr-pic1.jpg"
         			 alt=""
         			 className="fpImg"
       				 />
			        <Card.Title>A private Oceanside</Card.Title>
			        <Card.Text>
			          A private Oceanside getaway in the beautiful area of Bauan, Batangas. Three bedrooms with en suite bathrooms.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			{/*Second Card*/}
			<Col className = "col-md-4 col-10 mx-auto m-md-0 m-1">
				<Card className = "cardHighlight">
			      <Card.Body>
			      <img
         			 src="https://i.ibb.co/VtgLWrF/325055873-1250971605496154-8500555115834550570-n.jpg"
         			 alt=""
         			 className="fpImg"
       				 />
			        <Card.Title>Discover Scuba Diving and Free dive</Card.Title>
			        <Card.Text>
			          With our quick and easy introduction to what it takes to explore the underwater world. To sign up you must be at least 10 years old and in Reasonable Physical Health. During this experience we will be diving from the beach swimming out to the prestine Coral Reef of Binukbok with our coaches.
			        </Card.Text>
			      </Card.Body>
			    </Card>
		    </Col>
			   {/*Third Card*/}
		    <Col className = "col-md-4 col-10 mx-auto m-md-0 m-1">
		    	<Card className = "cardHighlight">
		          <Card.Body>
		          <img
         			 src="https://i.ibb.co/b2ybJhM/325427598-736108131357639-2485767037308867059-n.jpg"
         			 alt=""
         			 className="fpImg"
       				 />
		            <Card.Title className="asd">Be Part of Our Community</Card.Title>
		            <Card.Text>
		              Join our community and be part of our friendly divers tribe here in OCEAN VIEW- by the cabin.
		            </Card.Text>
		          </Card.Body>
		        </Card>
	        </Col>
		</Row>

	)
};