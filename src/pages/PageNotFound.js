import {Container, Row , Col, Image, Div} from 'react-bootstrap';
import {Route} from 'react-router-dom';
import {Link} from 'react-router-dom';



export default function PagesNotFound() {
	
	return (
		<Container>
			<Row className="text-center mx-auto">
				<Col>
				<Image className = "img-fluid img404" src ="https://img.freepik.com/free-vector/oops-404-error-with-broken-robot-concept-illustration_114360-1932.jpg?w=740&t=st=1676431384~exp=1676431984~hmac=f600af4890fbed41d2bf7a6c22ad708b06f52040795cad4c0936d7ab66c9f4cc"/>
				<h4 className ="text-center text-muted">Go back to the <Link to ={"/"}>homepage</Link>.</h4>

				</Col>
					
			</Row>

		</Container>

		)

}
