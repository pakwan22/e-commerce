import {Fragment} from 'react';
import {Container} from 'react-bootstrap';

import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'



export default function home () {
	
	return (
		<Fragment>
			<Container className="mt-5">	
				<Banner/>
			</Container>
			<Container className="mt-5">
				<Highlights/>
			</Container>
		</Fragment>
		)

}