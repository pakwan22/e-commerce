import RoomCards from '../components/RoomCard.js'
import {Container,div, Card, Button, Form, Row, Col, InputGroup} from 'react-bootstrap';
import {Fragment, useEffect, useState} from 'react';




export default function RoomSearch () {

	const[roomSearch, setSearchRooms] = useState([]);
	const[search, setSearch] = useState([]);
	const[searchValue, setSearchValue] = useState([]);


	function display (){
		fetch(`${process.env.REACT_APP_API_URL}/room/searchAll`)
		.then(result => result.json())
		.then(data => {
			setSearchRooms(data.map(rooms=>{
				console.log(data)
				return(<RoomCards key ={rooms._id} searchRoomsProp = {rooms}/>)
			}))
		})
	}


	useEffect(()=>{
		display();
	}, [])

	useEffect(()=>{
		if (search==""){
			display();
		}else{
			fetch(`${process.env.REACT_APP_API_URL}/room/searchAllRooms/${search}`)
			.then(result => result.json())
			.then(data => {
				if (data){
					setSearchRooms(data.map(rooms=>{
					return(<RoomCards key ={rooms._id} searchRoomProp = {rooms}/>)	
					}))
				}
					
			})
		}

		
	}, [search])



	return ( 
		<Fragment>
			<Container className="">
			<InputGroup 
				size="sm" 
				className="my-3"
				value = {searchValue}
				placeholder="Search"
				onChange ={event=> setSearch(event.target.value)}
				>
		        <InputGroup.Text id="inputGroup-sizing-sm"></InputGroup.Text>
		        <Form.Control
		          aria-label="Small"
		          aria-describedby="inputGroup-sizing-sm"
		        />
		        <InputGroup.Text id="inputGroup-sizing-sm"><h5></h5></InputGroup.Text>
		      </InputGroup>
			</Container>
			<div className="col-12 mt-2 d-flex flex-wrap justify-content-center">
						{roomSearch}
			</div>
		</Fragment>
		)
}

